## Über uns

Die Sensirion AG ist ein aus der ETH Zürich hervorgegangenes, innovatives und international tätiges Hightech Unternehmen mit Sitz am Zürichsee mit über 500 Mitarbeitenden.

High Tech Made in Switzerland - Die weltweit führende Sensortechnologie zur Messung von Feuchtigkeit, Gas- und Flüssigkeitsdurchflüsse sowie Differenzdrucke sorgen für mehr Lebensqualität sowie den schonenden Umgang mit Ressourcen.

Geforscht, entwickelt und produziert wird im Headquarter in Stäfa (ZH). Der Vertrieb wird durch ein internationales Netzwerk mit Büros in den USA, Deutschland, China, Taiwan, Japan und Korea unterstützt. Die Kombination aus Teamgeist und technologischer Kompetenzführerschaft macht Sensirion zum bevorzugten Partner der Automobilbranche, Medizinaltechnik, Gebäudetechnologie, Prozessautomatisierung und Konsumgüterindustrie.

## Schwerpunkt

Die Forschung und Entwicklung von neuen, digitalen Mikrosensorprodukten sowie von Produktionsprozessen zur Massenfertigung dieser Produkte bieten zahlreiche Herausforderungen.

Sensirion ermöglicht Physikern/-innen und Ingenieuren/-innen in der F&E und in unserem Produktionsumfeld, sowohl mit interdisziplinärer Arbeit als auch mit der Lösung hoch spezialisierter Technologiefragen, Mikrosensortechnik an der Weltspitze mit zu gestalten.
