// Contains static information about the tasks of the board members.
// Markdown can be used to style the text.

const boardTaskDescriptions = {
  President: {
    de:
      'Der Präsident ist für die Koordination der Arbeit des gesamten Vorstandes verantwortlich und repräsentiert den Fachverein nach aussen. Er beruft die Sitzungen und Generalversammlungen ein und delegiert die Arbeit an die einzelnen Ressorts. Er ist das Bindeglied zum Dachverband, dem VSETH.',
    en:
      'The president is responsible for coordinating the work of the entire board and represents the association to the outside world. He convenes the meetings and general assemblies and delegates the work to the individual departments. He is the link to the umbrella organization, the VSETH.',
  },

  Quaestor: {
    de:
      'Der Quästor ist verantwortlich für die finanziellen Angelegenheiten des AMIV, namentlich für die Budgetierung und Rechnungsführung. Er verwaltet zudem die Vereinskonten und prüft die Rechnungen aller Kommissionen.',
    en:
      'The Quaestor is responsible for the financial affairs of AMIV, namely budgeting and accounting. He also manages the bank accounts of the association and checks the bills of all commissions.',
  },

  IT: {
    de:
      'Der IT Vorstand ist verantwortlich für alle Technik im AMIV. Dazu gehören zum Beispiel Webseite, Apps, Bierautomat, PVK Tool, Server, Workstations und die Organisation des Codingweekends. Alles was die Website angeht: webmaster@amiv.ethz.ch',
    en:
      'The head of IT is responsible for all technical stuff at AMIV. This includes maintaining the website, apps, beer dispenser, PVK tool, servers, workstations and leading the IT team. Everything regarding the website: webmaster@amiv.ethz.ch',
  },

  Information: {
    de:
      'Der Informationsvorstand koordiniert diverse Informationskanäle wie die Announce, Facebook und die gute alte Homepage, und stellt sicher, dass alle Studenten gut über des Geschehen in und um ihren Lieblingsverein informiert bleiben. Nicht zuletzt gehört auch das pflichtbewusste Beantworten von allem, was am Ende des Tages im info@amiv.ethz.ch Postfach übrigbleibt, dazu. Ausserdem leitet er das Design-Team, welches sich um das ganze AMIV Werbematerial kümmert.',
    en:
      'The head of communications coordinates various information channels such as the Announce, Facebook and the good old website, and ensures that all students stay well informed about the events in and around their favorite association. Last but not least, dutifully answering everything that remains at the end of the day in the info@amiv.ethz.ch mailbox is part of it. He also leads the design team, which takes care of the entire AMIV advertising material.',
  },

  'Event Planning': {
    de:
      'Neben einem anspruchsvollen und zeitintensivem Studium, das die ETH bekanntlich mit sich bringt, kommt das Sozialleben oft zu kurz. Deswegen geht das Ressort Kultur der Aufgabe nach, den Studenten ein abwechslungsreiches Freizeitangebot zu bieten. Die Kulturis organisieren diverse Veranstaltungen, bei denen unsere Mitglieder die Möglichkeit haben, sich gegenseitig kennenzulernen. Seit einem Jahr werden die Kultur-Vorstände vom Kulturteam bei der Event-Organisation unterstützt. In diesem Team kann jeder mitmachen, der Lust dazu hat. Zudem gibt es je nach Event noch zahlreiche Helfer, um alle anfallenden Arbeiten zu bewältigen. Interessenten melden sich bitte unter kultur@amiv.ethz.ch',
  },

  'University Policy': {
    de:
      'An der ETH haben die Studenten sehr viele Mitspracherechte – vielleicht mehr, als man gemeinhin so denkt. Die Hochschulpolitiker engagieren sich dabei (zusammen mit einem HoPo-Team pro Departement) in den Departements- und Unterrichtskonferenzen der beiden vom AMIV abgedeckten Departementen. Zudem sind sie Ansprechpartner für alle möglichen Studiumsbezogenen Fragen, organisieren Prüfungsvorbereitungskurse und führen Vorlesungs-Evaluationen mittels Semestersprechern sowie Tutorenabende für Studenten aus den tieferen Semestern durch. Hast du Fragen oder Anregungen? Melde dich ganz unverbindlich beim entsprechenden HoPo-Team unter  hopo-itet@amiv.ethz.ch oder hopo-mavt@amiv.ethz.ch',
  },

  'External Relations': {
    de:
      'Die Mitglieder des Ressorts External Relations bauen neue Kontakte zur Wirtschaft auf und pflegen bereits vorhandene. Hauptaufgabe ist die Beschaffung von Sponsoringgeldern und der Informationsaustausch zwischen Industrie und Studenten – um Mehrwert für die Studenten zu schaffen. Zusammen mit dem Präsidenten sind die Vorstände dieses Ressorts das Gesicht des AMIV nach aussen. Ausserdem organisieren sie jeweils im Herbstsemester die grosse AMIV Firmenkontaktmesse «AMIV Kontakt». Um auch während dem Semester interessante Exkursionen anbieten zu können, sind die beiden auf externe Mithilfe angewiesen. Hast du Lust, das ER-Team im Bereich Exkursionen, Kontakt oder Sponsoring zu unterstützen? Dann melde dich kurz unter  kontakt@amiv.ethz.ch',
  },

  Infrastructure: {
    de:
      'Antonia ist für sämtliche Räumlichkeiten des AMIV und der darin enthaltenen, dem Verein gehörenden, Einrichtung und deren Zustand verantwortlich. Die Gestaltung und Zuordnung im Kulturraumes, Regeln für den Aufenthaltsraum und die Getränkeversorgung beanspruchen die meiste Zeit ihrer Tätigkeit als Vorstand. Es freut sie jeweils, wenn sie sieht, wie AMIVler denn Müll trennen und sich um Ordnung und Sauberkeit bemühen. Für sämtliche Anliegen, Anreize und Ideen hat sie stets ein offenes Ohr und freut sich auf eine Mail von euch. infrastruktur@amiv.ethz.ch',
  },
};

export { boardTaskDescriptions };
