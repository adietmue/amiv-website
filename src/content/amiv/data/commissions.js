// Contains static information about our commissions.
// Markdown can be used to style the text.

const data = [
  {
    name: 'LIMES - Ladies in Mechanical and Electrical Studies',
    description: {
      en: 'Not a real description.',
    },
    website: 'https://limes.amiv.ethz.ch',
    email: 'limes@amiv.ethz.ch',
  },
  {
    name: 'Bastli',
    description: {
      de: `Das Elektroniklabor des AMIV von und für ETH Studenten bietet Euch kostenlose Arbeitsplätze und Werkzeuge um eure eigenen Projekte und Ideen umzusetzen.

          Wir sind für alle da und helfen euch gerne. Vorkentnisse werden keine benötigt, Hauptsache ihr habt Spass daran Dinge zu bauen ;)`,
    },
    website: 'https://bastli.ethz.ch',
    email: 'info@bastli.ethz.ch',
    phone: '+41 44 632 49 41',
  },
  {
    name: 'Blitz',
    website: 'https://blitz.ethz.ch',
    email: 'info@blitz.ethz.ch',
  },
];

export { data };
