// lecture names sorted by department and semester
export const lectures = {
  itet: {
    '1': ['Digitaltechnik', 'Analysis 1', 'Netzwerke und Schaltungen 1', 'Informatik 1'],
    '2': ['Koma'],
    '3': ['Physics 2'],
    '4': [],
    '5+': [],
  },
  mavt: {
    '1': [
      'Analysis 1',
      'Werkstoffe und Fertigung 1',
      'Lineare Algebra 1',
      'Chemie',
      'Maschinenelemente',
    ],
    '2': ['Innovationsprozess'],
    '3': ['Dynamics', 'Thermodynamik 1', 'Philosophie'],
    '4': ['Fluiddynamik1', 'Thermodynamik 2'],
    '5+': [],
  },
};
