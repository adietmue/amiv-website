## Portrait

ABB ist weltweit führend in der Energie- und Automationstechnik. Das Unternehmen ermöglicht seinen Kunden in der Energieversorgung und der Industrie, ihre Leistung zu verbessern und gleichzeitig die Umweltbelastung zu reduzieren. ABB beschäftigt rund 130'000 Mitarbeitende in über 100 Länder.

In der Schweiz arbeiten rund 6'600 Mitarbeiter und Mitarbeiterinnen. In der Energietechnik arbeitet ABB Schweiz für Kunden, die in der Elektrizitäts-, Gas- oder Wasserversorgung tätig sind. ABB deckt fast die gesamte Palette der Stromübertragung und -verteilung von Kraftwerkkomponenten bis zur Steckdose ab. Dies umfasst Hoch- und Mittelspannungsschaltanlagen, Schaltanlagenkomponenten, Leistungshalbleiter sowie Transformatoren und Trafostationen. Die Automationstechnik von ABB Schweiz beliefert die Industrie in den Bereichen Zement, Metall- und Bergbau sowie Chemie, Pharma, Konsumgüter und Lebensmittel. Zu den Kunden gehören zudem Unternehmen aus der Fertigung, der Papier- und Druckindustrie sowie dem Maschinen-, Auto- und Schiffbau.

Im ABB-Forschungszentrum in Baden-Dättwil wird Innovation gross geschrieben: Die 170 Beschäftigten aus mehr als 25 Ländern arbeiten schon heute an der Lösung der Probleme von morgen. Denn für einen Technologiekonzern wie ABB sind erfolgreiche Innovationen überlebenswichtig. In der Schweiz wird überdurchschnittlich viel Kapital in die Forschung und Entwicklung investiert. Die Schwerpunkte im Corporate Research Center der Schweiz sind Automatisierung und Informatik, Materialien und Technologien für die Elektrotechnik, Leistungselektronik, Kommunikation sowie Elektronik, Messtechnik und Sensorik.

## Standorte in der Schweiz

Baden, Baden-Dättwil, Lenzburg, Turgi, Klingnau, Zürich-Oerlikon, Schaffhausen, Genf

## Gesuchte Fachrichtungen

* Elektrotechnik und Informationstechnologie
* Informatik
* MTEC
* Maschinenbau und Verfahrenstechnik
* Materialwissenschaft
* Physik

## Möglichkeiten

* Direkteinstieg
* Verschiedene Trainee Programme
* Praktikum
* Bachelor- und Masterarbeiten

[Alles zu Ihrer Karriere](http://www.abb.ch/karriere)
